//Написать программу, которая генерирует случайным образом 40 паролей\
заданной пользователем длины.\
В пароль должны входить латинские буквы в верхнем и нижнем регистрах, а также цифры.\
Пароли должны выводиться в два столбца по 20 штук.
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define N 40 //количество паролей
int generatePass(int len)
{
    if (len<3) return 0;
    int figure=0, smallLetter=0,bigLetter=0;
    int symbolType;//0-цифра, 1-строчная буква, 2-заглавная буква
    int alfabet=26;//26 длина латинского алфавита
    int j;
    for(j=0;j<len;j++)
    {
        symbolType=rand()%3;
        if ((len-j)==2)
        {
            if (figure==0 && smallLetter==0)
            {
                symbolType=rand()%2;
            }
            else if (smallLetter==0 && bigLetter==0)
            {
                symbolType=rand()%2+1;
            }
            else if (figure==0 && bigLetter==0)
            {
                symbolType=rand()%3;
                if (symbolType==1)
                {
                    symbolType=2;
                }
            }

        }
        else if ((len-j)==1)
        {
            if (figure==0)
            {
                symbolType=0;
            }
            if(smallLetter==0)
            {
                symbolType=1;
            }
            if (bigLetter==0)
            {
                symbolType=2;
            }
        }
        if (symbolType==0)
        {
            figure++;
            putchar(rand()%10+'0');
        }
        else if (symbolType==1)
        {
            smallLetter++;
            putchar(rand()%alfabet+'a');
        }
        else if (symbolType==2)
        {
            bigLetter++;
            putchar(rand()%alfabet+'A');
        }

    }
    return 1;


}

int main()
{
    srand(time(0));
    int symbolType;//0-цифра, 1-строчная буква, 2-заглавная буква
    int len;//длина пароля
    int alfabet=26;//26 длина латинского алфавита
    //int numSymbol=10;//10 цифры
    int i,j;
    printf("Enter lenght of your password: ");
    if (!scanf("%d",&len))
    {
        printf("ERROR");
        return 1;
    }
    for(i=0;i<N;i++)
    {
        if (!generatePass(len))
        {
            printf("Your password shorter 3");
            return 1;
        }


        if (i%2==1)
        {
            putchar('\n');
        }
        else
        {
            putchar(' ');
        }

    }
return 0;
}



